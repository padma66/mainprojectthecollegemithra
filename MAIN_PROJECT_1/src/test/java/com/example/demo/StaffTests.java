package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

	import org.junit.jupiter.api.Test;
	import org.mockito.Mock;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
	import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
	import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
	import org.springframework.test.annotation.Rollback;

import com.example.demo.entity.Staff;
import com.example.demo.repository.StaffRepository;

	@DataJpaTest
	@AutoConfigureTestDatabase(replace = Replace.NONE)
	@Rollback(value=false)

	public class StaffTests {

	    @Autowired
	    private StaffRepository repo;
	    
	    @Mock
	    Staff staff;
	    
	    @Test
	    public void testCreateStaff() {
	    	Staff staff = new Staff();
	    	staff.setId(90);
	    	staff.setStaffId(8);
	    	staff.setdId(4);
	    	staff.setsName("YAMUNARANI");
	    	staff.setSalary((float)30000);
	    	
	         assertNotNull(repo.save(staff));
	             
	        }
	    @Test
	    public void testFindName() {
	    	Staff staff = new Staff();
	    	staff.setsName("Sony");
	         assertNotNull(staff.getsName());
	    }
	    @Test
	    public void testFindStaffId() {
	    	Staff staff = new Staff();
	    	staff.setStaffId(6);
	         assertNotNull(staff.getStaffId());
	    }
	    @Test
	    public void testFindSalary() {
	    	Staff staff = new Staff();
	    	staff.setSalary((float)60000);
	         assertNotNull(staff.getSalary());
	    }
	    @Test
	    public void testFinddId() {
	    	Staff staff = new Staff();
	    	staff.setdId(7);
	         assertNotNull(staff.getdId());
	    }
	    @Test
	    public void testCreateStaff2() {
	    	Staff staff = new Staff();
	    	staff.setId(90);
	    	staff.setStaffId(6);
	    	staff.setdId(4);
	    	staff.setsName("raja");
	    	staff.setSalary((float)30000);
	    	
	         assertNotNull(repo.save(staff));
	             
	        }
	    @Test
	    public void testFindName2() {
	    	Staff staff = new Staff();
	    	staff.setsName("Sitha");
	         assertNotNull(staff.getsName());
	    }
	    @Test
	    public void testFindStaffId2() {
	    	Staff staff = new Staff();
	    	staff.setStaffId(9);
	         assertNotNull(staff.getStaffId());
	    }
	    @Test
	    public void testFindSalary2() {
	    	Staff staff = new Staff();
	    	staff.setSalary((float)40000);
	         assertNotNull(staff.getSalary());
	    }
	    @Test
	    public void testFinddId2() {
	    	Staff staff = new Staff();
	    	staff.setdId(11);
	         assertNotNull(staff.getdId());
	    }
	   
}
