package com.example.demo;

import static org.junit.jupiter.api.Assertions.assertNotNull;

	import org.junit.jupiter.api.Test;
	import org.mockito.Mock;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
	import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
	import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
	import org.springframework.test.annotation.Rollback;

import com.example.demo.entity.Department;
import com.example.demo.repository.DepartmentRepository;

	@DataJpaTest
	@AutoConfigureTestDatabase(replace = Replace.NONE)
	@Rollback(value=false)

	public class DepartmentTests {

	    @Autowired
	    private DepartmentRepository repo;
	    
	    @Mock
	    Department department;
	    
	    @Test
	    public void testCreateProduct() {
	    	Department department = new Department();
	    	department.setId(90);
	    	department.setDepartmentId(8);
	    	department.setD_name("ece");
	    	department.setHODName("YAMUNARANI");
	    	department.setTotalStaff("30");
	    	department.setTotalStudents("450");
	    	
	         assertNotNull(repo.save(department));
	             
	        }
	    @Test
	    public void testFindId() {
	    	Department department = new Department();
	    	department.setId(7);
	         assertNotNull(department.getId());
	    }
	    @Test
	    public void testFindDepartmentId() {
	    	Department department = new Department();
	    	department.setDepartmentId(6);
	         assertNotNull(department.getDepartmentId());
	    }
	    @Test
	    public void testFindD_name() {
	    	Department department = new Department();
	    	department.setD_name("civil");
	         assertNotNull(department.getD_name());
	    }
	    @Test
	    public void testFindHODName() {
	    	Department department = new Department();
	    	department.setHODName("Mr.Chandra");
	         assertNotNull(department.getHODName());
	    }
	    @Test
	    public void testFindTotalStaff() {
	    	Department department = new Department();
	    	department.setTotalStaff("25");
	         assertNotNull(department.getTotalStaff());
	    }
	   
	    @Test
	    public void testFindTotalStudents() {
	    	Department department = new Department();
	    	department.setTotalStudents("30");
	         assertNotNull(department.getTotalStudents());
	    }
	   
	   
}

