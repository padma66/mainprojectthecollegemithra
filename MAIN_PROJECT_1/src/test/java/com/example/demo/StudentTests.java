package com.example.demo;

	import static org.junit.jupiter.api.Assertions.assertNotNull;
	import org.junit.jupiter.api.Test;
	import org.mockito.Mock;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
	import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
	import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
	import org.springframework.test.annotation.Rollback;

import com.example.demo.entity.Student;
import com.example.demo.repository.StudentRepository;

	@DataJpaTest
	@AutoConfigureTestDatabase(replace = Replace.NONE)
	@Rollback(false)
	public class StudentTests {

	    @Autowired
	    private StudentRepository repo;
	    
	    @Mock
	    Student student;
	    
	    @Test
	    public void testCreateStudent() {
	    	Student student = new Student();
	    	student.setId(4);
	    	student.setStudent_id(4);
	    	student.setName("padmavathi");
	    	student.setGender("female");
	    	student.setJoiningdate("22-09-2021");
	         assertNotNull(repo.save(student));
	    }   
	           
	    @Test
	    public void testFindId() {
	    	Student student = new Student();
	    	student.setId(2);
	         assertNotNull(student.getId());
	    }
	    @Test
	    public void testFindStudent_id() {
	    	Student student = new Student();
	    	student.setStudent_id(3);
	         assertNotNull(student.getStudent_id());
	    }
	    @Test
	    public void testFindName() {
	    	Student student = new Student();
	    	student.setName("vennela");
	         assertNotNull(student.getName());
	    }
	    @Test
	    public void testFindGender() {
	    	Student student = new Student();
	    	student.setGender("female");
	         assertNotNull(student.getGender());
	    }
	    @Test
	    public void testFindJoiningdate() {
	    	Student student = new Student();
	    	student.setJoiningdate("23-09-2021");
	         assertNotNull(student.getJoiningdate());
	    }
	             
}
