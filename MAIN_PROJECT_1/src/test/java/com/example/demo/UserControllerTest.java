package com.example.demo;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import com.example.demo.controller.HomeController;
import com.example.demo.service.CustomUserDetails;

import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.junit.jupiter.SpringExtension;
@ExtendWith(MockitoExtension.class)
@ExtendWith(SpringExtension.class)
public class UserControllerTest {
    
	   @InjectMocks
	    HomeController homeController;
	    @Mock
	    private CustomUserDetails userservice;
  
	@Test
    public void viewHomePage() {
		homeController.viewHomePage();
    }
	@Test
    public void viewHomePage1() {
		homeController.viewHomePage1();
	}
	@Test
    public void viewHomePage2() {
		homeController.viewHomePage2();
    }
	
}
