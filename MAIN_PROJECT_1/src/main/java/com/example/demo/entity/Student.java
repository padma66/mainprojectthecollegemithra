package com.example.demo.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="student",schema="thecollegemithra")
public class Student {
	@Id
	@GeneratedValue

private Integer id;
private Integer student_id;
private String name;
private String gender;
private String joiningdate;
private String branch;

public Student() {
	super();
}

public Student(Integer id, Integer student_id, String name, String gender, String joiningdate, String branch) {
	super();
	this.id = id;
	this.student_id = student_id;
	this.name = name;
	this.gender = gender;
	this.joiningdate = joiningdate;
	this.branch = branch;
}

public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}

public Integer getStudent_id() {
	return student_id;
}
public void setStudent_id(Integer student_id) {
	this.student_id = student_id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getJoiningdate() {
	return joiningdate;
}
public void setJoiningdate(String joiningdate) {
	this.joiningdate = joiningdate;
}
public String getBranch() {
	return branch;
}
public void setBranch(String branch) {
	this.branch = branch;
}
}


