package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="staff",schema="thecollegemithra")
public class Staff {
	@Id
	@GeneratedValue
	public Integer id;
	
	private Integer staffId;
	@Column(name="department_Id")
	private int dId;
	
  @Column(name="staff_name")
   private String sName;
   private Float salary;
   public Staff() {}
public Staff(Integer id, Integer staffId, int dId, String sName, Float salary) {
	super();
	this.id = id;
	this.staffId = staffId;
	this.dId = dId;
	this.sName = sName;
	this.salary = salary;
}
public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
public Integer getStaffId() {
	return staffId;
}
public void setStaffId(Integer staffId) {
	this.staffId = staffId;
}
public int getdId() {
	return dId;
}
public void setdId(int dId) {
	this.dId = dId;
}
public String getsName() {
	return sName;
}
public void setsName(String sName) {
	this.sName = sName;
}
public Float getSalary() {
	return salary;
}
public void setSalary(Float salary) {
	this.salary = salary;
}
   
}