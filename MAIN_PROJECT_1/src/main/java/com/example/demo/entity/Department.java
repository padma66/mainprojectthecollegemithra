package com.example.demo.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="departments",schema="thecollegemithra")
public class Department{
	@Id
	@GeneratedValue
    private Integer id;
	private Integer departmentId;
	
	@Column(name="department_name")
private String d_name;
private String HODName;
private String totalStudents;
private String totalStaff;

public Department() {
	super();
}

public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
public Integer getDepartmentId() {
	return departmentId;
}

public void setDepartmentId(Integer departmentId) {
	this.departmentId = departmentId;
}
public String getD_name() {
	return d_name;
}
public void setD_name(String d_name) {
	this.d_name = d_name;
}
public String getHODName() {
	return HODName;
}
public void setHODName(String hODName) {
	HODName = hODName;
}
public String getTotalStudents() {
	return totalStudents;
}
public void setTotalStudents(String totalStudents) {
	this.totalStudents = totalStudents;
}
public String getTotalStaff() {
	return totalStaff;
}
public void setTotalStaff(String totalStaff) {
	this.totalStaff = totalStaff;
}

}
