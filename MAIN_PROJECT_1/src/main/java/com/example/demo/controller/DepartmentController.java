package com.example.demo.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import com.example.demo.entity.Department;
import com.example.demo.service.DepartmentService;

@Controller

public class DepartmentController {
	@Autowired
	private DepartmentService departmentService;
	
	//display list of departments
	@GetMapping("/department")
    public String viewHomePage(Model model) {
	model.addAttribute("listDepartments", departmentService.getAllDepartments());
	return "department";
}
	@GetMapping("/showNewDepartmentForm")
	public String showNewDepartmentForm(Model model) {
		Department department=new Department();
		model.addAttribute("department", department);
		return "new_department";
		
	}
	@PostMapping("/saveDepartment")
	public String saveDepartment(@ModelAttribute("department") Department department) {
		departmentService.saveDepartment(department);
		return "redirect:/department";
	}
	@GetMapping("/UpdateDepartment/{id}")
	public String showDepartmentFormForUpdate(@PathVariable(value="id")int id, Model model) {
		Department department=departmentService.getDepartmentById(id);
		model.addAttribute("department", department);
		return "update_department";
		
	}
	@GetMapping("/deleteDepartment/{id}")
	public String deleteDepartment(@PathVariable(value="id") int id) {
		this.departmentService.deleteDepartmentById(id);
		return "redirect:/department";
	}
	
}