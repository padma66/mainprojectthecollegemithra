package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
@Controller
public class HomeController {
	
	@Autowired
	private UserRepository repo;
	
	@GetMapping("/home")
    public String viewHomePage() {
	return "home";
	}
	
	@GetMapping("/home1")
    public String viewHomePage1() {
	return "home1";
	}

	@GetMapping("/home2")
    public String viewHomePage2() {
	return "home2";
	}
	
	@GetMapping("/about")
    public String viewAbout() {
	return "about";
	}
	
	@GetMapping("/contact")
    public String viewContact() {
	return "contact";
	}
	
	@GetMapping("/adminHome")
    public String viewAdminHome(Model model) {
	return "adminHome";
	}


	@GetMapping("/Admin_login")
    public String viewAdminLogin(Model model) {
	return "Admin_login";
	}
	@GetMapping("/departments")
    public String viewDepartments(Model model) {
	return "departments";
	}
	
	@RequestMapping("/loginAdmin")
	public String loginUser(@RequestParam(value="email")String email, @RequestParam(value="password")String password) {
		User user=repo.findByEmail(email);
		if(user != null && user.getRole() != null && user.getRole().equals("admin")) {
			return "adminHome";
		}

		return "deerror";
	}


}
