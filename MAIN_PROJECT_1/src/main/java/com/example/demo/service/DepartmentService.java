package com.example.demo.service;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Department;
@Repository
public interface DepartmentService extends JpaRepository<Department, Integer> {
List<Department> getAllDepartments();
void saveDepartment(Department department);
Department getDepartmentById(int id);
void deleteDepartmentById(int id);
List<Department> listAll();

}
