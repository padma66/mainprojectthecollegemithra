package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Staff;

public interface StaffService {
	List<Staff> getAllStaff();
	void saveStaff(Staff staff);
	Staff getStaffById(int id);
	void deleteStaffById(int id);
}
