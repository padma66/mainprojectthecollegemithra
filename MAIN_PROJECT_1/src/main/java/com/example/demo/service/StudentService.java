package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.entity.Student;
@Service
public interface StudentService {
List<Student> getAllStudents();
void saveStudent(Student student);
Student getStudentById(int id);
void deleteStudentById(int id);
}
