package com.example.demo.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Staff;
import com.example.demo.repository.StaffRepository;
@Service

public class StaffServiceImpl implements StaffService{
@Autowired
private StaffRepository staffRepository;

	@Override
	public List<Staff> getAllStaff() {
		
		return staffRepository.findAll();
	}
	@Override
	public void saveStaff(Staff staff) {
		this.staffRepository.save(staff);
		
	}
	@Override
	public Staff getStaffById(int id) {
		Optional<Staff> optional=staffRepository.findById(id);
		Staff staff=null;
		if(optional.isPresent()) {
			staff=optional.get();
		}
		else {
			throw new RuntimeException("Staff not found for id::" + id);
		}
		return staff;
	}
	@Override
	public void deleteStaffById(int id) {
		this.staffRepository.deleteById(id);
		
	}

}
